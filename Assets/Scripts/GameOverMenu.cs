using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameOverMenu : MonoBehaviour
{
    int score = 0;
    int highScore = 0;
    float gameTime = 0f;

    public TextMeshProUGUI textoScore;
    public TextMeshProUGUI textoHighScore;
    public TextMeshProUGUI textoTempo;

    public string MostrarTempoSobrevivido(float gameTime) {

        float minutos = Mathf.FloorToInt(gameTime / 60);
        float segundos = Mathf.FloorToInt(gameTime % 60);

        return string.Format("Tempo sobrevivido: {0:00}:{1:00}", minutos, segundos);

    }

    // Start is called before the first frame update
    void Start()
    {
        score = PlayerPrefs.GetInt("score", 0);
        highScore = PlayerPrefs.GetInt("highScore", 0);
        gameTime = PlayerPrefs.GetFloat("gameTime", 0f);
        textoScore.text = "Pontuação obtida: " + score.ToString();
        textoHighScore.text = "Pontuação mais alta: " + highScore.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayAgain() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    public void ReturnMainMenu() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 2);
    }


    public void QuitGame() {
        Application.Quit();
    }


}
