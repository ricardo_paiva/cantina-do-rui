using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class GameStatus : MonoBehaviour
{   
    public static GameStatus instance;

    int score = 0;
    int highScore = 0;
    float remainingTime = 45f;
    float gameTime = 0f;

    bool isGameOver = false;

    public GameObject firstPersonController;

    public GameObject painelInstrucoes;

    public TextMeshProUGUI textoScore;
    public TextMeshProUGUI textoTempo;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public float GetGameTime() {
        return gameTime;
    }

    public int GetPontuacaoJogo() {
        return score;
    }

    void Start()
    {
        highScore = PlayerPrefs.GetInt("highScore", 0);
        textoScore.text = "Score: " + score.ToString();
        textoTempo.text = "Tempo: " +  remainingTime.ToString();
    }

    void Update()
    {
        if (remainingTime > 0 && !isGameOver)
        {
            remainingTime -= Time.deltaTime;
            gameTime += Time.deltaTime;
            AtualizarTimer();
        }
        else
        {
            // Game Over
            TriggerEndGame();
        }

        if (Input.GetKey(KeyCode.I))
        {
            painelInstrucoes.SetActive(true);
        } else
        {
            painelInstrucoes.SetActive(false);
        }
    }

    public void AdicionarPontos(int pontos)
    {
        score += pontos;
        textoScore.text = "Score: " + score.ToString();
        
        if (score > highScore)
        {
            highScore = score;
            PlayerPrefs.SetInt("highScore", highScore);
        }
    }

    public void AtualizarTimer() {

        float minutos = Mathf.FloorToInt(remainingTime / 60);
        float segundos = Mathf.FloorToInt(remainingTime % 60);

        textoTempo.text = string.Format("Tempo: {0:00}:{1:00}", minutos, segundos);

    }

    public void IncrementarTimer(float segundos) {
        remainingTime += segundos;
    }

    public void TriggerEndGame() {
        isGameOver = true;
        Debug.Log("Game Over");
        firstPersonController.SetActive(false);

        PlayerPrefs.SetInt("score", score);
        PlayerPrefs.SetFloat("gameTime", gameTime);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        // Carregar tela de Game Over
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void OpenInstructions() {
        painelInstrucoes.SetActive(true);
    }
}
